# Mini-Reset.css

## What it is

A 3 line reset of some of the more common elements in web development, with no fluff or elements you probably won't use. That's it.

## To install

Either:

* Copy and paste the CSS in
* Save the file, and put it at the top of your external stylesheet links.

Done.